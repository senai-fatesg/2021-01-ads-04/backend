package com.franciscocalaca.backend.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.franciscocalaca.backend.bo.ContactBo;
import com.franciscocalaca.backend.dao.ContactDao;
import com.franciscocalaca.backend.dto.ContactDto;
import com.franciscocalaca.backend.entidade.Contact;

@RestController
@RequestMapping("/contact")
public class ContactRest {

	@Autowired
	private ContactDao contactDao;
	
	@Autowired
	private ContactBo contactBo;
	
	@GetMapping
	public List<Contact> get(){
		return contactDao.findAll();
	}
	
	@PostMapping
	public void post(@RequestBody ContactDto contactDto) {
		contactBo.salvar(contactDto);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") Integer id) {
		contactDao.deleteById(id);
	}
}
