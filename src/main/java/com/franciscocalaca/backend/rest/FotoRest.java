package com.franciscocalaca.backend.rest;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.franciscocalaca.backend.dao.ImagemDao;
import com.franciscocalaca.backend.entidade.Imagem;

@RestController
@RequestMapping("/foto")
public class FotoRest {

	@Value("${ads04.diretorioImagens}")
	private String diretorioImagens;
	
	@Autowired
	private ImagemDao imagemDao;
	

	@GetMapping("/{id}")
	public void downloadFile(@PathVariable("id") Integer id, HttpServletResponse response, HttpServletRequest request) throws IOException{
		File file = new File(diretorioImagens, id + ".png");
		byte[] fileContent = Files.readAllBytes(file.toPath());
		
		response.setContentType("image/png");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.getOutputStream().write(fileContent);		
	}

	@GetMapping("/db/{id}")
	public void downloadFileDb(@PathVariable("id") Long id, HttpServletResponse response, HttpServletRequest request) throws IOException{
		Optional<Imagem> imgOpt = imagemDao.findById(id);
		
		if(imgOpt.isPresent()) {
			Imagem img = imgOpt.get();

			response.setContentType("image/png");
			response.setHeader("Pragma", "no-cache");
			response.setHeader("Cache-Control", "no-cache");
			byte [] fileContent = Base64.getDecoder().decode(img.getConteudo());
			response.getOutputStream().write(fileContent);		
		}
		
	}

}
