package com.franciscocalaca.backend.dto;

import com.franciscocalaca.backend.entidade.Contact;

public class ContactDto {
	private Contact contact;
	
	private String foto;

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}
	
	
}
