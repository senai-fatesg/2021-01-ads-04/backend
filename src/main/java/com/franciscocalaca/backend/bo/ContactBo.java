package com.franciscocalaca.backend.bo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;

import javax.management.RuntimeErrorException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.franciscocalaca.backend.dao.ContactDao;
import com.franciscocalaca.backend.dao.ImagemDao;
import com.franciscocalaca.backend.dto.ContactDto;
import com.franciscocalaca.backend.entidade.BackendException;
import com.franciscocalaca.backend.entidade.Contact;

@Service
public class ContactBo {

	@Autowired
	private ImagemDao imagemDao;

	@Autowired
	private ContactDao contactDao;
	
	@Value("${ads04.diretorioImagens}")
	private String diretorioImagens;

	public void salvar(ContactDto contactDto) {
		Contact contact = contactDao.save(contactDto.getContact());
		File file = new File(diretorioImagens, contact.getId() + ".png");
		
////		salva no banco
//		Imagem img = new Imagem();
//		img.setId((long)contact.getId());
//		img.setConteudo(contactDto.getFoto());
//		img.setData(new Date());
//		imagemDao.save(img);
		
//		salva no disco
		try {
			FileOutputStream fos = new FileOutputStream(file);
			String base64 = contactDto.getFoto();
			byte [] dados = Base64.getDecoder().decode(base64);
			fos.write(dados);
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new BackendException(e.getMessage(), e);
		}
		
	}
	
}
