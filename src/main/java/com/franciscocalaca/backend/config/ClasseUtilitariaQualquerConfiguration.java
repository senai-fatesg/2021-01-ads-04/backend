package com.franciscocalaca.backend.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ClasseUtilitariaQualquerConfiguration {
	
	@Bean
	public ClasseUtilitariaQualquer getCuq() {
		ClasseUtilitariaQualquer cuq = new ClasseUtilitariaQualquer();
		return cuq;
	}

}
