package com.franciscocalaca.backend.config;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.franciscocalaca.backend.entidade.BackendException;
import com.franciscocalaca.backend.entidade.Erro;

@ControllerAdvice
public class ExceptionHandlingController extends ResponseEntityExceptionHandler{


	@ExceptionHandler(Exception.class)
	public ResponseEntity<Erro> handleError(HttpServletRequest req, Exception e) {
		Erro erro = new Erro();
		
		if(e instanceof ConstraintViolationException) {
			ConstraintViolationException cve = (ConstraintViolationException) e;
			StringBuffer msg = new StringBuffer();
			for(ConstraintViolation cv : cve.getConstraintViolations()) {
				msg.append(cv.getMessage());
				msg.append(";");
			}
			erro.setMensagem(msg.toString());
		}
		
		return new ResponseEntity<Erro>(erro, HttpStatus.INTERNAL_SERVER_ERROR);
	}


}
