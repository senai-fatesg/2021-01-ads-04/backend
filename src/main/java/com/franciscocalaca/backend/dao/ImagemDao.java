package com.franciscocalaca.backend.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.franciscocalaca.backend.entidade.Imagem;

@Repository
public interface ImagemDao extends JpaRepository<Imagem, Long>{

}
