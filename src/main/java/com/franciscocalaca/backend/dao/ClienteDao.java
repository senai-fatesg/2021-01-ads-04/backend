package com.franciscocalaca.backend.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.franciscocalaca.backend.entidade.Cliente;

@Repository
public interface ClienteDao extends JpaRepository<Cliente, Long>{

}
